#include <Arduino.h>

#define R_LED 11
#define G_LED 9
#define B_LED 10

struct RGB
{
    unsigned char r, g, b;
};
unsigned char r, g, b;
void setup() {
    pinMode(R_LED, OUTPUT);
    pinMode(G_LED, OUTPUT);
    pinMode(B_LED, OUTPUT);
    Serial.begin(9600);
      
}

void loop() {
    if (Serial.available() > 0) {
        r = Serial.read();
        Serial.println(r, DEC);
        g = Serial.read();
        Serial.println(g, DEC);
        b = Serial.read();
        Serial.println(b, DEC);
        Serial.println();
        Serial.println();
    }
    analogWrite(R_LED, 255 - r);
    analogWrite(G_LED, 255 - g);
    analogWrite(B_LED, 255 - b);
    delay(1000);
}
