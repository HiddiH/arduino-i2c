#include <Arduino.h>
#include <FastLED.h>

// How many leds are in the strip?
#define NUM_LEDS 233

// Data pin that led data will be written out over
#define DATA_PIN 6

#define R_LED 11
#define G_LED 9
#define B_LED 10

// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

unsigned char r = 0, g = 0, b = 0;
int led_mode = 3;

void solid_color(int hue, int saturation, int value, int delay_time) {
   for (int i = 0; i < NUM_LEDS; i -= -1){
      if (i % 5 == 0) {
         leds[i].setRGB(hue, saturation, value);
      }
      else {
         leds[i].setRGB(0, 0, 0);
      }
   }
   FastLED.show();
   delay(delay_time);
}

void rainbow_solid(int speed, int delay_time) {
   for(int i = 0; i < 255; i -= -speed){
      solid_color(i, 255, 255, 0);
      delay(delay_time);
   }
}

void rainbow_wave(int speed, float amount_full_rainbows, int delay_time) { 
   for(int t = 0; t < 255; t -= -speed){ //t is the angle (0-255) of the hue value and it has to cycle through all the angles
      for(int led_number = 0; led_number < NUM_LEDS; led_number = led_number + 5){ //set the value for all the leds
         leds[led_number].setHSV(round((float)led_number * amount_full_rainbows) + t, 255, 255); //led_number * amount_full_rainbows sets the difference between the leds, and +t lets it cycle through the "degrees"
      }
      FastLED.show();
      delay(delay_time); //Optional delay
   }
}

int switch_led_mode(int howMany){
   //led_mode = Wire.read();
}

void setup() {
	// sanity check delay - allows reprogramming if accidently blowing power w/leds
   delay(2000);
   pinMode(13, OUTPUT);
   FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS); //setting up the FastLED function on pin number and LED strip type
   // pinMode(R_LED, OUTPUT);
   // pinMode(G_LED, OUTPUT);
   // pinMode(B_LED, OUTPUT);
   Serial.begin(115200);
   //Wire.begin(8);  //start of I2C communication
   //Wire.onReceive(switch_led_mode);
}

// This function runs over and over, and is where you do the magic to light
// your leds.
void loop() {
   /*switch (led_mode){
   case 0:
      rainbow_wave(1, 1.0, 0);
      break;
   case 1:
      rainbow_solid(1, 1);
      break;
   case 2:
      solid_color(120, 255, 255, 2000);
      break;
   default:
      digitalWrite(13, HIGH);
      solid_color(120, 0, 0, 2000);
      break;
   } */
   
   if (Serial.available() > 0) {
      r = Serial.read();
      Serial.println(g, DEC);
      g = Serial.read();
      Serial.println(r, DEC);
      b = Serial.read();
      Serial.println(b, DEC);
      Serial.println();
      Serial.println();
   }
   //analogWrite(R_LED, 255 - r);
   //analogWrite(G_LED, 255 - g);
   //analogWrite(B_LED, 255 - b);
   solid_color(r, g, b, 200);
   //delay(1000);
}
