#!/bin/bash
stty 115200 < /dev/ttyUSB0
while :
do
    echo "Red:"
    read red
    echo "Green:"
    read green
    echo "Blue:"
    read blue
    echo -ne \\x$red\\x$green\\x$blue > /dev/ttyUSB0
    echo "Send the code to the Arduino"
done
