<html>
    <head>
        <style>
            .succes-box {
                background-color: #afffaf;
                max-width: max-content;
                padding: 10px 20px;
                margin: 0 auto;
                border-radius: 10px;
                font-size: 20px;
                font-weight: bold;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/@jaames/iro@5"></script>
    </head>
    <body>
        <?php
            require 'main.php';
        ?>
        <div id="picker" />
        <form id="form" method="post">
            <input type="text" name="red" style="display: none;"><br>
            <input type="text" name="green" style="display: none;"><br>
            <input type="text" name="blue" style="display: none;"><br>
        </form>
        <button type="button" id="submit-button">Change color</button>
        <script>
            let colorPicker = new iro.ColorPicker('#picker');
            document.querySelector('#submit-button').onclick = ev => {
                let hexStr = colorPicker.color.hexString;
                hexStr = hexStr.replace("#", "");
                let r = hexStr.substr(0,2);
                let g = hexStr.substr(2,2);
                let b = hexStr.substr(4,2);

                const form = document.querySelector('#form');
                form.elements["red"].value = r;
                form.elements["green"].value = g;
                form.elements["blue"].value = b;
                form.submit();
            };
        </script>
    </body>
</html>
